# Flyway <img src="https://documentation.red-gate.com/download/attachments/138346876/FD?version=3&modificationDate=1633982869952&api=v2" width="50" height="50">

[Página oficial](https://flywaydb.org/)

[Documentación](https://documentation.red-gate.com/fd/command-line-184127404.html)

## Instalación y configuración
- Nos conectamos a la terminal si tienen problema de permisos se pueden conectar como usuario root
    ```bash
    sail root-shell
    ```

- Actualizamos e instalamos paquetes faltantes 
    ```
    apt update && apt upgrade
    apt install wget vim nano
    ```

- En la carpeta donde queramos bajar el archivo ejecututamos
    ```bash
    wget -qO- https://repo1.maven.org/maven2/org/flywaydb/flyway-commandline/9.14.1/flyway-commandline-9.14.1-linux-x64.tar.gz | tar xvz

    ```

- Podemos renombrar la carpeta 
    ```bash
    mv flyway-9.14.1/ flyway
    ```

- creamos un enlace simbolico para poder tener el comando directo
    ```bash
    ln -s `pwd`/flyway/flyway /usr/local/bin/
    ```

- En la carpeta donde instalamos flyway editamos el archivo flyway.conf
    ```bash
    nano conf/flyway.conf
    ```

- Editamos el archivo de configuración y verificamos que corresponda con la informacion de el .env
    ```text
    # Url para conexion a la base host:puerto/base_datos
    flyway.url=jdbc:postgresql://pgsql:5432/<base_datos>
    # Usuario de la base de datos
    flyway.user=<user>
    # Contraseña de la base de datos
    flyway.password=<password>
    # Ubicación donde se encuentran los archivos sql 
    flyway.locations=filesystem:/var/www/html/database/sql
    ```

- Verificamos la instalación 
    ```bash
    flyway --version
    ```

# Comandos para usar flyway
| Comando  | Uso |
| ------   |  ------ |
| baseline | Crea una linea base sobre datos ya existentes, excluyendo todas las migraciones hasta la version base |
| clean    | Borra todos los objetos |
| info     | Visualiza los detalles y estaod de las migraciones |
| migrate  | Realiza los cambios a nuestra base de datos |
| repair   | Repara el historial de la tabla de esquemas |
| validate | Valida as migraciones aplicadas contra las disponibles |


```bash
    flyway <comando>
```


# Ejercicio

- [ ] Creamos la carpeta sql en la ruta var/www/html/database 
    ```bash
    mkdir -p /var/www/html/database/sql
    cd /var/www/html/database/sql
    ```

- [ ] Creamos el archivo V001__CambiosTablaElemento.sql con el siguiente contenido: (respetar el nombrado V###__Descripcion.sql)
    ```sql
    ALTER TABLE elemento ALTER COLUMN usuario_id DROP NOT NULL;
    ALTER TABLE elemento ALTER COLUMN tipo_elemento_id DROP NOT NULL;
    ALTER TABLE elemento ALTER COLUMN elem_fecha_creacion DROP NOT NULL;
    ALTER TABLE elemento ALTER COLUMN elem_ies_hash DROP NOT NULL;
    ALTER TABLE elemento ALTER COLUMN elem_ies_nombre DROP NOT NULL;
    ALTER TABLE elemento ALTER COLUMN elem_estatus DROP NOT NULL;
    ```
- [ ] Verificamos que se muestre la migracion
    ```bash
    flyway info
    ```

- [ ] Como ya existe la base de datos antes de migrar los cambios necesitamos crear nuestra linea base
    ```bash
    flyway baseline
    ```

- [ ] Aplicamos las migraciones
    ```bash
    flyway migrate
    ```

- [ ] Mostramos la información de las migraciones  
    ```bash
    flyway info
    ```

- Como creamos el baseline se ignora el archivo V001__CambiosTablaElementos.sql y muestra un  mensaje parecido al que se muestra a continuación: 

    ```
    +-----------+---------+-----------------------+----------+---------------------+--------------------+----------+
    | Category  | Version | Description           | Type     | Installed On        | State              | Undoable |
    +-----------+---------+-----------------------+----------+---------------------+--------------------+----------+
    |           | 1       | << Flyway Baseline >> | BASELINE | YYYY-MM-DD HH:MM:SS | Baseline           | No       |
    | Versioned | 001     | cambios elemento      | SQL      |                     | Ignored (Baseline) | No       |
    +-----------+---------+-----------------------+----------+---------------------+--------------------+----------+

    ```

- [ ] Cambiamos el nombre del archivo por V002__CambiosTablaElemento.sql
    ```bash
    mv V001__CambiosTablaElemento.sql V002__CambiosTablaElemento.sql
    ```

- [ ] Aplicamos las migraciones
    ```bash
    flyway migrate
    ```

- [ ] Mostramos la información de las migraciones 
    ```bash
    flyway info
    ```