# Clonar proyecto con SAIL en WSL

Los prerequisitos es contar con Windows 10 o superior y tener instalado Docker Desktop con WSL2 habilitado

- [ ] Clonar el proyecto
```
git clone
```
- [ ] Moverse a la carpeta del proyecto
```
cd carpeta-proyecto
```
- [ ] Ejecutar el siguiente comando para instalar las dependencias y así poder utilizar el comando sail. Este paso es necesario si no se tiene instalado php y composer en el WSL directamente.
```
docker run --rm \
    -u "$(id -u):$(id -g)" \
    -v "$(pwd):/var/www/html" \
    -w /var/www/html \
    laravelsail/php82-composer:latest \
    composer install --ignore-platform-reqs
```
- [ ] Crear el archivo .env
```
cp .env.example .env
```
- [ ] Editar el archivo .env y modificar los siguientes valores:
```
DB_CONNECTION=pgsql
DB_HOST=pgsql
DB_PORT=5432
DB_DATABASE=dbname
DB_USERNAME=username
DB_PASSWORD=password

CACHE_DRIVER=redis

REDIS_HOST=redis
```

- [ ] Asignar los permisos a la carpeta del proyecto
```
cd ..
chmod -R gu+w carpeta-proyecto
chmod -R guo+w carpeta-proyecto
cd carpeta-proyecto
```

- [ ] Ejecutar el comando para crear y levantar los servicios

```
sail up -d
```

Si el comando sail no se encuentra debe agregarse el alias al archivo ~/.bashrc

```
vi ~/.bashrc
```

Agregar la siguiente linea al final del archivo; moverse al final del archivo, presionar la tecla i y pegar la linea; al finalizar presionar la tecla Esc y escribir :wq para guardar y salir:
```
alias sail='[ -f sail ] && sh sail || sh vendor/bin/sail'
```
- [ ] Generar la llave de la aplicación
```
sail artisan key:generate
```
- [ ] Instalar las dependencias de javascript
```
sail npm install
```
- [ ] Compilar los assets CSS y JS
```
sail npm run dev
```
- [ ] Ejecutar las migraciones de laravel
```
sail artisan migrate
```

- [ ] Llegado a este punto ya debe poder ver el proyecto en el navegador en http://localhost

- [ ] Posteriormente (por ejemplo después de reiniciar el equipo y se requiera trabajar en el proyecto) se deben ejecutar los comandos:
```
sail up -d
sail npm run dev
```

- [ ] Ejemplos de algunos comandos útiles durante el desarrollo:
```
# Acceder a la base de datos en la terminal:
sail psql
```
```
# Limpiar el caché de la configuración de laravel
sail artisan config:cache
```
