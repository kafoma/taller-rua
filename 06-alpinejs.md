# Alpine.js
![alpine.js](https://alpinejs.dev/alpine_long.svg)
[Página oficial](https://alpinejs.dev/)

[Curso Alpine JS](https://www.youtube.com/watch?v=hn28X9p2-7U&list=PLZ2ovOgdI-kVcpcljnRe7heDP-YVrk1ki)

## Verificar la instalación 
Para verificar que este instado alpine tenemos que ver el archivo `vite.config.js` en la seccion de input verificamos que se encuentra `resources/js/app.js`
```js
plugins: [
        laravel({
            input: [
                'resources/css/app.css',
                'resources/js/app.js',
            ],
            ...
        }),
    ],
```

y en el archivo de app.js verificamos que se encuentre las siguientes lineas: 

```js
...

import Alpine from 'alpinejs';
import focus from '@alpinejs/focus';
window.Alpine = Alpine;
Alpine.plugin(focus);
Alpine.start();

...

```


- Para inicializar alpine utilizamos la directiva `x-data` y todo el elemento que tenga esta etiqueta lo considera como un componente (todas las funcionalidades de alpine aplicarían sobre esta etiqueta).

```html
<div x-data>
    ... 
</div>
```

- Agregar el texto a un elemento.
```html
<span x-text="new Date().getFullYear()">
    ... 
</span>
```

- Agregar un evento del navegador a un elemento.  
```html
<button x-on:click="">
   ... 
</button>

<button @click="">
</button>
```

- Alternar la visibilidad de un elemento.
```html
<div x-data="{open:false}">
<button @click="open=!open">Ver/Ocultar</button>
<p x-show="open">
	Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
</p>
```

- Ejecutar algún código cuando se inicializa el componente.
```html
<div x-init="fecha = new Date()">
    <p x-text="fecha.getFullYear()"></p>
</div>
```

- Sincronizar un dato con un elemento
```html
<div x-data="{ search: '' }">
  <input type="text" x-model="search"> 
  <p x-text="search"></p>
</div>

```
- Referencia a elementos directamente por sus claves especificadas usando la propiedad mágica $refs
```html
<div x-data="{ search: '' }">
  <input type="text" x-model="search"> 
  <button @click="$refs.busqueda.innerText=search">Enviar</button>
  <button @click="navigator.clipboard.writeText($refs.busqueda.value)">Copiar</button>
  
  <p x-ref="busqueda"></p>
</div>

``` 

- Iteraciones y Condicionales
	- Para las iteraciones ocuparemos la etiqueta `x-for` y una sintaxis * "([item], [index]) in [items]"`
	- Para el condicional if se ocupa la etiqueta `x-if`
	- Ambos deben tener la etiqueta `<template>`

```html
<!-- For -->
<template x-for="post in posts">
  <h2 x-text="post.title"></h2>
</template>

<ul x-data="{ colors: ['Red', 'Orange', 'Yellow'] }">
    <template x-for="(color, index) in colors">
        <li>
            <span x-text="`${index}: ${color}`"></span>
        </li>
    </template>
</ul>


<!-- IF -->
<template x-if="value > 0">
  <div>...</div>
</template>
``` 
- Métodos Mágicos
Son métodos que nos ayudan a 
	- `$el` acceder al elemento actual.
	- `$refs` acceder al elemento que tiene la etiqueta x-ref. 
	- `$dispatch` genera un evento de navegador.
	- `$root` Trae el elemento raiz el que contiene la etiqueta x-data.
	- `$data` Trae los datos del componente etiqueta x-data.
- Comunicación entre componentes
Para la comunicación entre componentes ocupamos el método mágico @dispatch generando un evento al navegador y en el otro componente escuchando ese evento
```html
<div x-data="{mensaje: null}">
    <input type="text" x-model="mensaje" @input="$dispatch('custom-event', mensaje)">
</div>
<div x-data="{mensaje: null}" @custom-event.window="mensaje=$event.detail">
    <input type="text" x-model="mensaje">
</div>
```