# Guía para uso de GIT
## Requisitos y guías 
- [ ] [Instalar git en su computadora](https://git-scm.com/downloads)
- [ ] [Tener una cuenta en GitLab](https://gitlab.com)
- [ ] [Guía oficial](https://git-scm.com/book/es/v2)
- [ ] [Tutoriales Atlassian](https://www.atlassian.com/es/git/tutorials)
- [ ] [Guía para comenzar con git](https://rogerdudler.github.io/git-guide/index.es.html)

## Que es Git
[Fundamentos de Git](https://book.git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Fundamentos-de-Git) 

### Zonas de Git

Recordemos las 3 zonas que utiliza git:
* Directorio de trabajo 
* Área de preparado
* Repositorio

> El directorio de trabajo: Será el directorio donde hayamos creado el repositorio, y en el tendremos los archivos que se quieran añadir al proyecto en algún momento determinado. Estos archivos aún no serán tenidos en cuenta por GIT para controlar sus cambios.
>
>Staging index: O área de preparación, es un área temporal de GIT donde se guardarán los archivos que están a punto de ser enviados al repositorio.
>
> El repositorio: También llamado HEAD, será la zona donde los archivos del proyecto estarán almacenados, ordenados y controlados para ser recuperados en cualquier momento.


### Estado de los archivos

> Recuerda que cada archivo de tu repositorio puede tener dos estados: 
> rastreados y sin rastrear. 
> 
> Los archivos rastreados (tracked files en inglés) son todos aquellos  
> archivos que estaban en la última instantánea del proyecto; pueden ser 
> archivos sin modificar, modificados o preparados. 
> 
> Los archivos sin rastrear 
> son todos los demás - cualquier otro archivo en tu directorio de trabajo 
> que no estaba en tu última instantánea y que no está en el área de 
> preparación (staging area). 
> 
> Cuando clonas por primera vez un repositorio, 
> todos tus archivos estarán rastreados y sin modificar pues acabas de 
> sacarlos y aun no han sido editados.

## Trabajando con GIT

### Revisar la instalación 

En una terminal ejecutar
para verificar que git este instalado.
```sh
git --version
```

### Configuración global de Git
Lo primero que deberás hacer cuando instales Git es establecer tu nombre de usuario y dirección de correo electrónico. Esto es importante porque los "commits" de Git usan esta información, y es introducida de manera inmutable en los commits que envías:
```bash
git config --global user.name "<nombre>"
git config --global user.email "<correo>"
```

Verificar que se guardaron los datos
```bash
git config --global --list
```

### Creación de un nuevo repositorio. 
Para crear un nuevo repositorio 

Descargar el proyecto de un repositorio remoto y subir nuevos archivos:
```bash
git clone git@gitlab.com:comando132/rua.git
cd rua
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main
```

Subir una carpeta existente a un repositorio vacio:
```bash
cd <folder>
git init --initial-branch=main
git remote add origin git@gitlab.com:comando132/rua.git
git add .
git commit -m "Initial commit"
git push -u origin main
```

### Comprobar el estatus de los archivos 
Para comprobar el estado de nuestros archivos utilizamos:

```bash
git status
git diff
```

El modo de trabajo a utilizar en git es el siguiente: 

Crearemos y modificaremos los archivos que tenemos en el directorio de trabajo, posteriormente pasaremos al staging index aquellos archivos del directorio de trabajo que queramos controlar con GIT, y después confirmaremos los cambios pasando los archivos del staging index al repositorio de GIT, para así ser controlados definitivamente.

### Agregar cambios
Para agregar los cambios a nuestra area de preparado (staging index) se puede utilizar cualquiera de estos comandos:
```bash
git add <filename>
git add .
git add -A
```

### Confirmar cambios
```bash
git commit -m "mensaje descriptivo de los cambios realizados"
```

### historial de commits.

```bash
git log
git log --oneline
git log --oneline --all --graph --decorate
```

### Cambiar de ramas o restaurar archivos 

```bash
git checkout <id> 
git checkout <rama> 
```

### Deshacer cambios 
El comando git reset te permite RESTABLECER tu estado actual a un estado específico. Puedes restablecer el estado de archivos específicos, así como el de todo una rama. Esto es útil si aún no has subido tu commit a un repositorio remoto. 

```bash
git reset
git reset --soft
git reset --hard
```

Pero si ya has subido tu commit a un repositorio remoto, se recomienda que no uses git reset, ya que reescribe el historial de commits. 

En su lugar es mejor usar git revert, que deshace los cambios realizados por un commit anterior creando un commit completamente nuevo, todo esto sin alterar el historial de commits.

```bash
git revert <id_commit>
```

## Ramas
[Documentación](https://git-scm.com/book/es/v2/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F)
### Operaciones con ramas
Ver las ramas que hay en el proyecto 
```bash
git branch
```

Crear una rama

```bash
git branch <nombre>
```

Cambiarte a una rama

```bash
git checkout <nombre>
```

Renombrar una rama
```bash
git branch -m <nombre-rama> <nuevo-nombre>
```

Eliminar rama en el repositorio local.
```bash
git branch -d <nombre-rama>
```

Diferencia entre ramas 
```bash
git checkout main
git diff <rama>
```

## Fusionar ramas
Debes posicionarte en la rama donde se desean incorporar los cambios e indicarle la rama donde estan los cambios

```bash
git checkout main
git merge <rama>
```

## Repositorios Remotos

- [ ] Generar una llave SSH 
```bash
ssh-keygen -t rsa -C "<correo_electronico>" -b 4096
```

>Al ejecutar el comando te permite elegir la ruta donde se guardara la llave, si dejas vacío el campo y presionas <enter> lo guardará en la ruta de home, también te permite poner una contraseña a la llave si lo deseas
>
>Las llaves ssh generadas se almacenan por defecto en la carpeta oculta ssh localizada
en el directorio home. Si no cambiaste la ruta de almacenamiento. 

  Posiciónate en la ruta.
para verificar que se hayan creado las llaves.

```bash
cd ~/.ssh
cat ~/.ssh/id_rsa.pub
```

- [ ] Copia la llave que has generado en la primera parte y agrégala a las llaves permitidas de tu perfil de Gitlab. De esta manera GitLab ya te permite ver y hacer cambios en el repositorio remoto desde tu computadora.
  
  ![Gitlab SSH Keys](assets/img/gitlab_ssh_keys.gif)

Para vincular el repositorio local con el que ha sido creado en la plataforma de Gitlab, es necesario emplear la URL del repositorio remoto, este parámetro se obtiene de la página principal de tu proyecto en Gitlab. 

- [ ] Utiliza el comando git remote para vincular tu repositorio con el repositorio remoto
```bash
git remote add origin <url-git>
```

### Operaciones para repositorios remotos 

Subir los cambios locales al repositorio remoto

```bash
git push -u origin <rama_remota>
```

Descargar los cambios remotos a local
```bash
git pull
```

Ver las ramas que solo existen en remoto
```bash
git branch -r
```

Subir una rama local al repositorio 
```bash
git push -u origin <nombre-rama-remota>
```  

Bajar una rama y borrar la rama en remoto
```bash
git push origin :<rama-remota>
```



## Guardar Temporalmente los cambios
Al modificar archivos puedes guardar temporalmente los cambio con el siguiente comando
```bash
git stash
```
`con la bandera -u incluimos los archivos sin seguimiento.`
>
> Puedes tener varios stash

Es recomendable comentar los stash con una descripción con el comando
```
git stash save "mensaje"
```

Volver aplicar los cambios 
```bash
git stash pop
git stash apply
```
> El primer comando aplica los cambios en el codigo pero eliminará el stash.
>
> El segundo aplica los cambios en el código en el que estás trabajando y conservarlos en tu stash. 
> Si ejecutas git stash pop aplica el último stash 

Ver la lista de stash
```bash 
git stash list
```

Puedes elegir el stash que deseas volver a aplicar poniendo su identificador
```bash
git stash pop stash@{<id>>}
```

Ver las diferencias de un stash resumen
```bash
git stash show
```

Ver las diferencias de un stash 
```bash
git stash show -p
```
`con la bandera -p  Ver todas las diferencias de un stash.`

Borrar un stash
```bash
git stash drop stash@{1}
```

Borrar todos los stash
```bash
git stash clear
```

# Ejercicio

- [ ] Verificar la instalación y configurar usuario.
```bash
git --version
git config --global user.name "<username>"
git config --global user.email "<email>"
git config --global --list
```

- [ ] Crear el repositorio.
```bash
git init --initial-branch=main
```

- [ ]  Comprobar el estado de los archivos.
```bash
git status
```

- [ ]  Agregar todos los archivos a la zona de ensayo.
```bash
git add -A
```


- [ ] Guardar los archivos en el repositorio.
```bash
git commit -m "Base del framework"
```

- [ ] Crear los archivos y agregarlos al repositorio. 

PrincipalController.php

- [ ] Modificar los archivos. 

- [ ] Ver las diferencias.
```bash
git diff PrincipalController.php
```

- [ ] Ver las diferencias 
```bash
git diff
```

- [ ] Visualizar el historial de cambios de tu repositorio.
```bash
git log
git log --oneline
git log --oneline --all --graph --decorate
```

- [ ] Copia el ID del primer commit y trae a tu copia de trabajo la primera versión del historial de cambios. y visualiza tus archivos.
```bash
git checkout <id>
```

- [ ] Para regresar a la última versión del historial de cambios. 
```bash
git checkout main
```

- [ ] Modificar los archivos y guardarlos en el repositorio. 

- [ ] Ejecuta el comando git revert pasando como parámetro HEAD para revertir el último commit que realizaste.
```bash
git revert HEAD
```

- [ ] Lista nuevamente el historial.
```bash
git log --oneline --all --graph --decorate
```

- [ ] Modificar los archivos y guardarlos en el repositorio. 
- [ ] Cuando se requiere deshacer un commit sin dejar rastro, puedes utilizar el comando git reset, este comando necesita un id del commit al que vas a resetear el historial. Lista el historial de commits y copia el ID del commit previo al actual.
```bash
git log --oneline
git reset --hard <id_commit>
```

- [ ] Modificar los archivos y guardarlos en el repositorio. 
- [ ] Copia nuevamente el ID del commit anterior y ejecuta el comando git reset con la bandera --soft. Este comando es ideal cuando quieres deshacer un commit, pero conservar los cambios en el archivo para seguir modificándolo.
```bash
git log --oneline
git reset --soft <id_commit>
```
## Remoto
- [ ] Crear un nuevo repositorio remoto en la plataforma de Gitlab.
- [ ] Agregar el repositorio remoto a nuestro repositorio local
```bash
git remote add origin <url-git>
```

- [ ] Importar el contenido del repositorio local al repositorio remoto.
```bash
git push -u origin --all
git push origin main
```

- [ ] El archivo .gitignore, este archivo se emplea en git para definir los archivos y carpetas que no se guardan en el repositorio, este archivo es muy útil cuando se trabaja con sistemas de gestión de dependencias como composer que generan los archivos.
Este archivo no afecta a los que ya se encuentran dentro del repositorio, estos continuarán sincronizándose con el repositorio.
```bash
touch .gitignore
# Archivos que no se subirán al repositorio
*.cache
*.tmp
/tmp
/logs
```

- [ ] Comparte la URL de tu repositorio con algún compañero y permite que este genere cambios en el mismo otorgándole permisos de “maintainer”. desde la página principal de tu proyecto en gitlab.
- [ ] Solicita la URL de su repositorio y clona el repositorio con ayuda del comando git clone, asegúrate de NO clonar el repositorio de tu compañero dentro de tu repositorio.
```bash
git clone <url-git>
```

## Ramas
- [ ] Lista las ramas existentes en el repositorio.
```bash
git branch
```

- [ ] Crea una nueva rama llamada “pruebas” y cambiate a ella.
```bash
git branch pruebas
git branch
git checkout pruebas
git branch
```

- [ ] Modifica algunos archivos y crea al menos uno nuevo, realiza varios commits en el repositorio local, la idea es tratar de cambiar el historial de versiones, para que no sea igual que el de la rama main, al final lista el historial para ver estos commits.
```bash
git add archivo
git commit -m "Nuevo archivo desde pruebas"
git log --oneline
```

- [ ] Subir la rama al repositorio remoto.
```bash
git branch -r
git push -u origin pruebas
```

- [ ] Cambiar a la rama main.
```bash
git checkout main
git log --oneline --decorate --graph
git log --oneline --decorate --graph --all
```

- [ ] Comparar los cambios de una rama con otra.
```bash
git diff pruebas
```

- [ ] Renombrar la rama a “fix”, esta acción solo puede realizarse si la rama se encuentra en un repositorio local. Si se desea renombrar una rama en el repositorio remoto debe eliminarse y crearse nuevamente.
```bash
git branch -m trabajo fix
git push -u origin fix
```

- [ ] Borrar la rama del repositorio remoto
```bash
git push origin :fix
```

- [ ] Borrar la rama local fix
```bash
git branch -d fix
git branch -D fix
```

## Merge
- [ ] Crear una rama "funcionalidad1"
- [ ] Modifica el archivo y sube los cambios.
- [ ] Crea otra rama “funcionalidad2” y cámbiate a esta nueva rama.
- [ ] Modifica el archivo y sube los cambios.
- [ ] Observa el historial de cambios de todas las ramas, observa los cambios realizados en cada rama.
```bash
git log --oneline –all --graph --decorate
```

- [ ] Posiciónate en la rama main  y realiza un “merge” de la rama que creaste. Para realizar el merge debes posicionarte sobre la rama a la que
deseas incorporar los cambios e indicarle que rama quieres fusionar.
```bash
git checkout main
git merge funcionalidad1
git log --oneline --all --graph –decorate
git merge funcionalidad2
git log --oneline --all --graph --decorate
```

- [ ] Posiciónate en la rama “funcionalidad1” y realiza un merge desde la rama main para actualizar el archivo de la rama “funcionalidad1”. Observa el historial de cambios y el archivo modificado, ahora tendrás la versión con todos los cambios.
```bash
git checkout funcionalidad1
git merge main
git log --oneline --all --graph --decorate
```
- [ ] Modifica el archivo y sube los cambios.
- [ ] Cámbiate a tu rama main y edita el archivo, guarda los cambios.
- [ ] Realiza un merge para fusionar los cambios realizados en la rama.
```bash
git merge funcionalidad1
```

- [ ]  En este caso se debería generar un conflicto ya que se han editado las mismas líneas de código, abre el archivo y resuelve el conflicto que se ha generado. En el archivo verás las líneas de código de ambos archivos marcando los cambios de cada una. Al guardar los cambios en el repositorio local se concluye el merge.
```bash
git add -a
git commit -m "Fusión de la funcionalidad1 a main"
git log --oneline --all --graph --decorate
```
