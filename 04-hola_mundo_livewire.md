# Primer componente livewire

- [ ] Comando para crear un nuevo componente llamado "counter". Este comando genera 2 archivos:

app/Htp/Livewire/Counter.php y resources/views/livewire/counter.blade.php


```
sail php artisan make:livewire counter
```

- [ ] En el archivo routes/web.php agregar al final:

```
Route::get('/counter', Counter::class);
```

- [ ] En el archivo app/Htp/Livewire/Counter.php. modificar el contenido del método render:

```
return view('livewire.counter')
            ->layout('layouts.guest');
```

- [ ] Modificar el contenido del archivo resources/views/livewire/counter.blade.php:

```
<div>
    <h1>¡Hola Mundo!</h1>
</div>
```

- [ ] Ver el componente en el navegador: http://localhost/counter

- [ ] Agregamos los scripts de Livewire en resources/views/layouts/guest.blade.php:


```
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Scripts -->
        @vite(['resources/css/app.css', 'resources/js/app.js'])

        <!-- Styles -->
        @livewireStyles
    </head>
    <body>
        <div class="font-sans text-gray-900 antialiased">
            {{ $slot }}
        </div>

        @livewireScripts
    </body>
</html>
```


- [ ] Para agregar la funcionalidad de contador, agregamos una propiedad pública y un método para incrementar el contador:

```
class Counter extends Component
{
    public $count = 0;

    public function increment()
    {
        $this->count++;
    }

    public function render()
    {
        return view('livewire.counter')
            ->layout('layouts.guest');
    }
}
```
- [ ] Modificar la vista para desplegar el contador y agregar un boton que realice el incremento:

```
<div style="text-align: center">
    <button wire:click="increment">+</button>
    <h1>{{ $count }}</h1>
</div>
```

- [ ] Ejercicio: Agregar un boton "-" que decremente el valor de $count
